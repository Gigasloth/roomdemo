package com.gigasloth.roomdemo.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import com.gigasloth.roomdemo.db.entity.StringData
import com.gigasloth.roomdemo.repository.StringDataRepository


class StringDataViewModel(application: Application) : AndroidViewModel(application) {
    val stringDataRepository = StringDataRepository()
    val allStringData = stringDataRepository.stringDataDao.getAllStringData()

    fun insert(stringData: StringData) {
        stringDataRepository.insertAsync(stringData)
    }
}