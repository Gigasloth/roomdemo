package com.gigasloth.roomdemo.dagger

import com.gigasloth.roomdemo.db.AppDb
import dagger.Component

@Component(modules = [DbModule::class])
interface AppComponent {
    fun inject(appDb: AppDb)
}