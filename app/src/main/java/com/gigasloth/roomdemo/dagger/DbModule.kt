package com.gigasloth.roomdemo.dagger

import android.arch.persistence.room.Room
import android.content.Context
import com.gigasloth.roomdemo.db.AppDb
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DbModule(private val context: Context) {
    @Provides
    fun provideAppContext() = context

    @Singleton
    @Provides
    fun provideAppDb(): AppDb =
            Room.databaseBuilder(context, AppDb::class.java, "DataDb")
                    // TODO: don't allow this in production, but for this test this should be fine
                    .allowMainThreadQueries()
                    .build()

    @Provides
    fun provideDataDao(database: AppDb) = database.getStringDataDao()
}