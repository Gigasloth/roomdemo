package com.gigasloth.roomdemo.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.gigasloth.roomdemo.R
import com.gigasloth.roomdemo.db.entity.StringData

class StringDataListAdapter(context: Context) : RecyclerView.Adapter<StringDataListAdapter.StringDataViewHolder>() {
    val inflater = LayoutInflater.from(context)
    val stringData: List<StringData> = arrayListOf()

    class StringDataViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvStringData = view.findViewById<TextView>(R.id.tv_string_data)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): StringDataViewHolder {
        return StringDataViewHolder(inflater.inflate(R.layout.view_holder_string_data, parent, false))
    }

    override fun onBindViewHolder(holder: StringDataViewHolder?, position: Int) {
        if (stringData.isNotEmpty()) {
            holder?.tvStringData?.text = stringData[position].data
        } else {
            holder?.tvStringData?.text = "No Data"
        }
    }

    override fun getItemCount(): Int {
        return stringData.size
    }

    fun setData(data: List<StringData>) {
        stringData
    }
}