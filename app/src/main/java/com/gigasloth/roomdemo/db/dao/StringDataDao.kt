package com.gigasloth.roomdemo.db.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.gigasloth.roomdemo.db.entity.StringData

interface StringDataDao {
    @Insert
    fun insert(stringData: StringData)

    @Delete
    fun delete(stringData: StringData)

    @Query("DELETE FROM string_data_table")
    fun deleteAll()

    @Query("SELECT * from string_data_table ORDER BY uuid ASC")
    fun getAllStringData() : LiveData<List<StringData>>
}