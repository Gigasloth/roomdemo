package com.gigasloth.roomdemo.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.gigasloth.roomdemo.db.dao.StringDataDao
import com.gigasloth.roomdemo.db.entity.StringData

@Database(entities = [StringData::class], version = 1, exportSchema = false)
abstract class AppDb : RoomDatabase() {
    abstract fun getStringDataDao(): StringDataDao
}