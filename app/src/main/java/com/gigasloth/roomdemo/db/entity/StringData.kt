package com.gigasloth.roomdemo.db.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "string_data_table")
data class StringData(@PrimaryKey(autoGenerate = true)
           @ColumnInfo(name = "string_data")
           val data: String)