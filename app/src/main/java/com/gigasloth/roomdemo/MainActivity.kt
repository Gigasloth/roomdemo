package com.gigasloth.roomdemo

import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.Button
import com.gigasloth.roomdemo.adapter.StringDataListAdapter
import com.gigasloth.roomdemo.db.entity.StringData
import com.gigasloth.roomdemo.viewmodel.StringDataViewModel
import java.util.*

class MainActivity : AppCompatActivity() {
    var recyclerView: RecyclerView? = null
    var stringDataAdapter: StringDataListAdapter? = null
    var viewModel: StringDataViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recyclerView = findViewById(R.id.rv_data)
        stringDataAdapter = StringDataListAdapter(this)
        recyclerView?.adapter = stringDataAdapter
        recyclerView?.layoutManager = LinearLayoutManager(this)

        viewModel = ViewModelProviders.of(this).get(StringDataViewModel::class.java)
        viewModel?.allStringData?.observe(this, android.arch.lifecycle.Observer {
            stringDataAdapter?.setData(it.orEmpty())
        })

        val addDataBtn = findViewById<Button>(R.id.btn_add_data)
        addDataBtn.setOnClickListener {
            val stringData = StringData(UUID.randomUUID().toString())
            viewModel?.insert(stringData)
        }
    }
}
