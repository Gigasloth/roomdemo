package com.gigasloth.roomdemo.repository

import android.arch.lifecycle.LiveData
import android.os.AsyncTask
import com.gigasloth.roomdemo.db.AppDb
import com.gigasloth.roomdemo.db.dao.StringDataDao
import com.gigasloth.roomdemo.db.entity.StringData
import javax.inject.Inject

class StringDataRepository {
    @Inject
    lateinit var appDb: AppDb

    @Inject
    // FIXME: kotlin.UninitializedPropertyAccessException: lateinit property stringDataDao has not been initialized
    lateinit var stringDataDao: StringDataDao

    val mAllStringData: LiveData<List<StringData>>
        get() = stringDataDao.getAllStringData()

    fun insertAsync(stringData: StringData) {
        this.InsertAsyncTask(stringDataDao).execute(stringData)
    }

    inner class InsertAsyncTask(dao: StringDataDao) : AsyncTask<StringData, Void, Void>() {
        private val mAsyncTaskDao = dao

        override fun doInBackground(vararg params: StringData): Void? {
            mAsyncTaskDao.insert(params[0])
            return null
        }
    }
}